/// <reference path="./typings/tsd.d.ts" />
import MarkBuilder = require("./libs/markBuilder");

let builder: MarkBuilder = new MarkBuilder("./docs-sources");

builder.build(function(err?: Error) {

    if (err) {

        console.log(err);
        return;
    }

    console.log("Done");
});
