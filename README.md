# MarkBuilder

一个可以将 Markdown 转换为 HTML 的项目构建器，支持目录生成。

## Usage

1.  初始化程序

    ```
    npm install
    tsd install
    tsc
    ```

2.  编写文档，将文档写在 `./docs-sources` 中

    支持如下特殊命令语法：

    -   在当前位置插入当页目录（此命令须独占一段）

        ```
        [markdown-menu]
        ```

    -   指定文档标题

        ```
        [markdown-title]: 这是一个段子
        ```

    -   指定模版，模版文件在 `./docs-sources/templates` 中，注意必须以 `.htm` 结尾

        ```
        [markdown-template]: xxxx.htm
        ```

    -   指定CSS样式文件，CSS文件在 `./docs-sources/styles` 中

        ```
        [markdown-style]: xxxx.css
        ```

3.  编译文档，输出到 `./dist` 目录下

    ```
    npm start
    ```

## Author

Angus.Fenying <[i.am.x.fenying@gmail.com](mailto:i.am.x.fenying@gmail.com)>
